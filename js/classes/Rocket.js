class Rocket extends _Entity
{

	constructor(timeToInput, type, game) 
	{
		super(game.world.centerX, game.world.height + 50, game);
		this.timeToInput = timeToInput;
		this.timeOfInput = 0;
		this.game = game;
		this.image = game.add.sprite(this.x, this.y, 'rocket_' + type);
		this.image.anchor.setTo(0.5, 0.5);
		this.readyForInput = false;
		this.game.input.onDown.add(this.clicked, this);

		if (type === "1")
			this.engineSfx = this.game.add.audio('engine_1');
		else
			this.engineSfx = this.game.add.audio('engine_2');

		this.launchSfx = this.game.add.audio('launch');
		this.launchSfx.volume = 0.9;

	}

	fire_rocket()
	{
		// This is done when the user clicks
		this.launchSfx.play();
		// WebAudio Awesomeness - Change pitch/pbr to a random value
		this.launchSfx._sound.playbackRate.value = Math.random() * (1.5 - 0.8) + 0.8;

		this.tween.pause();
		this.tween = this.game.add.tween(this.image).to({ y: 0 }, 350, Phaser.Easing.Linear.None, true);

		// WOO ARROW FUNCTION
		this.tween.onComplete.add(() => {this.remove_rocket()});
	}

	stage_rocket()
	{
		// Place the rocket just below the launch pad
		// let's the player know what's coming up next
		this.tween = this.game.add.tween(this.image).to({ x: this.game.world.centerX, y: 700 }, 175, Phaser.Easing.Elastic.Out, true);
		this.engineSfx.play();
	}

	ready_rocket()
	{
		// Move the rocket to the launch pad
		this.tween = this.game.add.tween(this.image).to({ x: this.game.world.centerX, y: 600 }, 300, Phaser.Easing.Elastic.Out, true);
		this.readyForInput = true;
	}

	explode()
	{
		// Fail.
		console.log('Explode');

		// this.engineGo.stop();
		var sfx = this.game.add.audio('explosion');
		sfx.play();

		var emitter = this.game.add.emitter(this.image.x, this.image.y, 100);
		emitter.makeParticles(['wreak_13', 'wreak_15', 'wreak_12', 'wreak_9', 'wreak_17']);
		emitter.gravity = 0;
		emitter.start(true, 3000, null, 10);

		this.remove_rocket();
	}

	perfect_star()
	{
		// Use this for perfect hits
		var emitter = this.game.add.emitter(this.image.x, this.image.y, 100);
		emitter.makeParticles(['star']);
		emitter.gravity = 0;
		emitter.start(true, 1000, null, 5);

		// Perfects get 2 points
		this.game.score += 2;
	}

	clicked()
	{
		// Do a check input
		if(this.readyForInput)
		{
			this.check_input();
		}
	}

	check_input()
	{
		this.readyForInput = false;

		// Was this input too early?
		if(this.timeOfInput <= 0)
		{
			// It was too early
			this.explode();
			console.log('early input');
			this.game.score -= 1;
			return;
		}

		// Was this input on ABOUT the first part of the beat?
		if(this.timeOfInput <= this.game.timePerBeat / 4);
		{
			this.game.inputInfo = "Time of input (sec): " + this.timeOfInput + "/" + (this.game.timePerBeat / 2);
			if(this.timeOfInput <= 0.06)
			{
				this.fire_rocket();
				return this.perfect_star();
			}
			this.game.score += 1;
			this.fire_rocket();
		}
	}

	remove_rocket()
	{
		// Remove the click event
		this.game.input.onDown.remove(this.clicked, this);

		// Destory the image/sprite
		this.image.destroy();

		// We're done here.
		delete this;
	}

	update(deltaTime)
	{
		// If we are ready for input, start counting down
		// the input window
		if(this.readyForInput)
		{
			// Remove time from the input window
			this.timeToInput -= (deltaTime / 1000);

			// The user must now input, start counting how low it
			// takes for them to input
			if(this.timeToInput <= 0)
			{
				this.timeOfInput += (deltaTime / 1000);
				// simple check if input is too late
				if(this.timeOfInput > this.game.timePerBeat / 2)
				{
					console.log('late input!');
					this.game.score -= 1;
					this.readyForInput = false;
					this.explode();
				}
			}
		}
	}

}
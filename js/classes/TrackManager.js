class TrackManager
{

	constructor(jsonObj, audioPlayer, cb)
	{
		// Load info from the beatmap
		this.bpm = jsonObj.bpm;
		this.bpb = jsonObj.bpb;
		this.bpms = jsonObj.bpms;
		this.bps = this.bpms / 1000.0;
		this.tracks = jsonObj.beatTraks;
		this.timePerBeat = 60 / this.bpm;

		// Set current beat to 0
		this.currentBeat = 0;

		// Set callback
		this.cb = cb;

		// Set the audio player
		this.audioPlayer = audioPlayer;
		this.audioPlayer.volume = 0.7;
		
		// Play!
		this.audioPlayer.play();
	}

	// Called internally to dispatch events to the callback
	triggerEventcb(eventName)
	{
		this.cb({
			name: eventName,
			beat: this.currentBeat
		});
	}

	// This calculates the current beat using the playtime of the song
	calcCurrentBeat()
	{
		return Math.floor(this.bpms * this.audioPlayer.currentTime);
	}

	// Called each tick to update and fire the beat events
	update()
	{
		// If we need to check events for the next beat
		if(this.currentBeat != this.calcCurrentBeat())
		{
			this.currentBeat = this.calcCurrentBeat();
			this.triggerEventcb('BEAT');
			// Now next check the beat maps
			for(var track of this.tracks)
			{
				// Does this track have an event?
				if(track[this.currentBeat] !== null)
				{
					this.triggerEventcb(track[this.currentBeat]);
				}
			}
		}
	}

}
(function(){

	var game = new Phaser.Game(1024, 720, Phaser.AUTO, 'BinaryBeatDown', { preload: preload, create: create, update: update, render: render });
	var launch_pad;

	function preload() 
	{

		// Init score
		game.score = 0;

		// Preload assets here...

		// Load launch pad
		game.load.image('launch_pad', 'assets/img/launch_pad.png');
		game.load.image('star', 'assets/img/star_gold.png');
		game.load.image('bg', 'assets/img/backdrop.png');

		console.log('loading rockets');

		// Load all rockets
		for (var i = 12; i >= 1; i--) {
			game.load.image('rocket_' + i, 'assets/img/ship/' + i + '.png');
		};

		console.log('loading meteors');

		// Load all meteors
		for (var i = 19; i >= 1; i--) {
			game.load.image('rock_' + i, 'assets/img/meteor/' + i + '.png');
		};

		console.log('loading parts');

		// Load all parts
		for (var i = 21; i >= 1; i--) {
			game.load.image('wreak_' + i, 'assets/img/wreak/' + i + '.png');
		};

		console.log('loading BeatMap');

		// Load our BeatMap!
		game.load.json('beatmap', 'assets/json/map.json');

		console.log('loading audio');

		// Load music
		game.load.audio('main_music', 'assets/music/Parallaxing_Atmosphere.wav');

		// Load engine sounds
		game.load.audio('engine_1', 'assets/music/engine_1.wav');
		game.load.audio('engine_2', 'assets/music/engine_2.wav');
		game.load.audio('launch', 'assets/music/launch.wav')
		game.load.audio('explosion', 'assets/music/explosion.wav');
		
		console.log('READY!');

	}

	// We'll declare our objects here
	var launch_pad;
	var music;
	var tracker;

	// Array of rockets in queue
	var rockets = [];
	var current_rocket = null;
	game.meteordrop = 5000;
	game.inputInfo = "";

	function processBeatEvent(event)
	{
		if(event.name === '1')
		{
			// Stage a new rocket type 1
			let temp = new Rocket(tracker.timePerBeat * 2, '1', game);
			rockets.push(temp);
			temp.stage_rocket();
			console.log("Push rocket 1");
		}
		else if(event.name === '2')
		{
			// Stage a new rocket type 2
			let temp = new Rocket(tracker.timePerBeat, '12', game);
			rockets.push(temp);
			temp.stage_rocket();
			console.log("Push rocket 2");
		}
		else if(event.name === 'ROCKET_1' || event.name === 'ROCKET_2')
		{
			current_rocket = rockets.pop();
			current_rocket.ready_rocket();
			console.log("pop rocket");
		}

		if(event.name === "SLOW")
		{
			console.log(event.name);
			game.add.tween(game).to( { bgX: 1, bgY: 1 }, 1500, Phaser.Easing.Linear.None, true);
			game.astemitter.start(false, 15000, 7000, 0);
		}
		if(event.name === "MEDIUM") 
		{
			console.log(event.name);
			game.add.tween(game).to( { bgX: -5, bgY: -5 }, 1500, Phaser.Easing.Linear.None, true);
			game.astemitter.start(false, 15000, 5000, 0);
		}
		if(event.name === "FAST")
		{
			console.log(event.name);
			game.add.tween(game).to( { bgX: 10, bgY: 10 }, 1500, Phaser.Easing.Linear.None, true);
			game.astemitter.start(false, 15000, 4000, 0);
		}
		if(event.name === "FASTEST")
		{
			console.log(event.name);
			game.add.tween(game).to( { bgX: -15, bgY: -15 }, 1500, Phaser.Easing.Linear.None, true);
			game.astemitter.start(false, 15000, 1000, 0);
		}
	}

	function create() 
	{

		//  To make the sprite move we need to enable Arcade Physics
		game.physics.startSystem(Phaser.Physics.ARCADE);
		game.starfield = game.add.tileSprite(0, 0, 1024, 720, 'bg');
		game.bgX = 0;
		game.bgY = 0;

		// Add our launch pad
		launch_pad = game.add.sprite(game.world.centerX, 600, 'launch_pad');
		launch_pad.anchor.set(0.5, 0.5);

		//  And enable the Sprite to have a physics body:
		game.physics.arcade.enable(launch_pad);

		// Make our music tracker
		music = game.add.audio('main_music');
		tracker = new TrackManager(game.cache.getJSON('beatmap') , music, processBeatEvent);

		// Save this for future reference
		game.timePerBeat = tracker.timePerBeat;

		// Debug
		console.log(tracker);

		// Setup ast emitter
		game.astemitter = this.game.add.emitter(500, -15, 100);
		game.astemitter.makeParticles(['rock_1', 'rock_12', 'rock_13', 'rock_4']);
		game.astemitter.gravity = 100;
		game.astemitter.start(false, 15000, game.meteordrop, 0);

	}

	function update() 
	{
		tracker.update();

		if(current_rocket)
		{
			current_rocket.update(game.time.elapsed);
		}

		// Move BG tile
		game.starfield.tilePosition.y += game.bgX;
		game.starfield.tilePosition.x += game.bgY;

		// rot the platfor
		launch_pad.angle += game.bgY - 2;

	}

	function render() 
	{

		game.debug.text("Binary Beat Down", 32, 32);
		game.debug.text(game.inputInfo, 32, 45);
		game.debug.text("Score: " + this.game.score, 32, 65);

	}

})();


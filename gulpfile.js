var gulp = require('gulp'),
	watch = require('gulp-watch'),
	babel = require("gulp-babel"),
	browsersync = require('browser-sync'),
	concat = require('gulp-concat');

gulp.task('default', function() {
  return gulp.src(['./js/classes/Entity.js', './js/classes/*.js', './js/bbd.js'])
	.pipe(babel())
	.pipe(concat('all.js'))
	.pipe(gulp.dest('./js/dist'));
});

gulp.watch('js/**/*.js', ['default']);
browsersync.init({server: {baseDir: "./"}});